# Rules

## Attendance
If a member of the party is not able to attend a session they should notify the current [GM](https://en.wikipedia.org/wiki/Gamemaster) about the situation before the session starts.
    In the case of the session starting without all party members & the GM present:
- If there are at least 60% of all party members present, the characters of the players absent will be proxied.
- Otherwise the session is called off.

## Membership

### Joining
A player can join up to the beginning of a campaign.

To join a player has to fulfill at least one of the following requirements:
- Be the first born male descendant of a previous player.
- Be a the legal spouse of a current player.
- Gain the votes of a supermajority of 75% of the current players in your favor (rounded up).
- A player has nominated you to be their replacement and you get a 50% favor vote from the remaing current players.

### Removal
Players can be removed from the party immediately under the following conditions:
- A unanimous vote from the rest of the players.
